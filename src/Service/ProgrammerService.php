<?php

namespace App\Service;


class ProgrammerService
{

    private $skills = ['writeCode', 'testCode', 'communicateWithTheManager'];

    public function getEmployeeSkills()
    {
        return $this->skills;
    }
}