<?php

namespace App\Service;


class ManagerService
{
    private $skills = ['setTasks'];

    public function getEmployeeSkills()
    {
        return $this->skills;
    }
}