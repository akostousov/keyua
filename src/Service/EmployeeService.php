<?php

namespace App\Service;

use App\Service\ProgrammerService;
use App\Service\DesignerService;
use App\Service\TesterService;
use App\Service\ManagerService;
class EmployeeService
{

    /**
     * @var ProgrammerService
     */
    private $programmer;

    /**
     * @var DesignerService
     */
    private $designer;

    /**
     * @var TesterService
     */
    private $tester;

    /**
     * @var ManagerService
     */
    private $manager;

    public function __construct(
        ProgrammerService $programmer,
        DesignerService $designer,
        TesterService $tester,
        ManagerService $manager
    )
    {
        $this->programmer = $programmer;
        $this->designer = $designer;
        $this->tester = $tester;
        $this->manager = $manager;
    }

    public function employeeSkills(string $employee)
    {
        if ($this->$employee) {
            return $this->$employee->getEmployeeSkills();
        } else {
            return ' The request failed ! ';
        }
    }

    public function canSkills(string $employee, string $skill)
    {
        $employeeSkills = $this->employeeSkills($employee);

        if (isset($employeeSkills)) {
            foreach ($employeeSkills as $employeeSkill) {
                if ($skill == $employeeSkill) {
                    return true;
                }
            }
        } else {
            return ' Incorrect request, employee not found ! ';
        }

    }
}