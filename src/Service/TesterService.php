<?php

namespace App\Service;


class TesterService
{
    private $skills = ['testCode', 'communicateWithTheManager', 'setTask'];

    public function getEmployeeSkills()
    {
        return $this->skills;
    }
}