<?php

namespace App\Service;


class DesignerService
{
    private $skills = ['draw', 'communicateWithTheManager'];

    public function getEmployeeSkills()
    {
        return $this->skills;
    }
}