<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use App\Service\EmployeeService;

class getSkillsEmployeeCommands extends Command
{
    /**
     * @var EmployeeService
     */
    private $employeeService;


    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('company:employee')
            ->addArgument('employee', InputArgument::REQUIRED, 'The position of employee.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $skills = $this->employeeService->employeeSkills($input->getArgument('employee'));
//        $skills = implode("\n", $skills);
//        $output->writeln($skills);

        foreach ($skills as $skill) {
            $output->write("- $skill \n");
        }

        return Command::SUCCESS;
    }



}