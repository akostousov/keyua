<?php

namespace App\Command;

use App\Service\EmployeeService;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;


class canSkillsEmployeeCommands extends Command
{
    /**
     * @var EmployeeService
     */
    private $employeeService;


    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('employee:can')
            ->addArgument('employee', InputArgument::REQUIRED, 'The position of employee.')
            ->addArgument('skill', InputArgument::REQUIRED, 'The skill of employee.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->employeeService->canSkills($input->getArgument('employee'), $input->getArgument('skill'));

        if ($result) {
            $output->write('true');
        } else {
            $output->write('false');
        }

        return Command::SUCCESS;
    }

}